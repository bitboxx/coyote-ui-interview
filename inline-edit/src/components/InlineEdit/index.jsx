import React, { Component, createRef } from 'react';
import propTypes from 'prop-types';

import './style.css';

const status = {
  PRISTINE: 'pristine',
  LOADING: 'loading',
  SUCCESS: 'success',
  ERROR: 'error',
};
class InlineEdit extends Component {
  constructor(props) {
    super(props);
    this.inputFieldRef = createRef();

    const { text } = this.props;
    this.state = {
      displayValue: text,
      inputValue: text,
      editMode: false,
    };
  }

  componentDidUpdate() {
    if (this.state.editMode) {
      this.inputFieldRef.current.focus();
    }
  }

  submissionHandler = () => {
    const { displayValue, inputValue } = this.state;
    if (displayValue !== inputValue) {
      this.setState({ displayValue: inputValue, editMode: false });
      this.props.onSubmit({ target: { value: inputValue } });
    } else {
      this.setState({ editMode: false });
    }
  };

  onClickHandler = () => {
    if (!this.state.editMode && this.props.status !== status.LOADING) {
      this.setState({ editMode: true });
    }
  };
  onChangeHandler = (event) =>
    this.setState({ inputValue: event.target.value });
  onBlurHandler = () => {
    this.submissionHandler();
  };
  onKeyDownHandler = (event) => {
    // Submit on enter
    if (event.nativeEvent.keyCode === 13) {
      event.preventDefault();
      this.submissionHandler();
    }
  };

  render() {
    const { status, errorMessage } = this.props;
    const { editMode, displayValue, inputValue } = this.state;
    return (
      <div className='InlineEdit'>
        <div
          className={`InlineEdit-fieldContainer InlineEdit-fieldContainer-${status}`}
        >
          <div className='InlineEdit-displayText' onClick={this.onClickHandler}>
            {displayValue}
          </div>
          {editMode && (
            <input
              ref={this.inputFieldRef}
              className={`InlineEdit-inputField`}
              type='text'
              value={inputValue}
              onKeyDown={this.onKeyDownHandler}
              onChange={this.onChangeHandler}
              onBlur={this.onBlurHandler}
            />
          )}
        </div>
        {errorMessage && (
          <div className='InlineEdit-errorMessage'>{errorMessage}</div>
        )}
      </div>
    );
  }
}
const noop = () => {};
InlineEdit.defaultProps = {
  errorMessage: null,
  onSubmit: noop,
  status: status.PRISTINE,
  text: '',
};
InlineEdit.propTypes = {
  errorMessage: propTypes.string,
  onSubmit: propTypes.func,
  status: propTypes.string,
  text: propTypes.string,
};

export default InlineEdit;
export { status };
