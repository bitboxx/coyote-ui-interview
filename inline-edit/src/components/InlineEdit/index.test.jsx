import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import InlineEdit from './index.jsx';

describe('InlineEdit', () => {
  it('should render', () => {
    const { container } = render(<InlineEdit />);
    expect(container).toMatchSnapshot();
  });

  it('should show text value', () => {
    const { queryByText } = render(<InlineEdit text='Have a cup of tea' />);
    expect(queryByText('Have a cup of tea')).toBeInTheDocument();
  });

  it('should show errorMessage only when errorMessage is defined', () => {
    expect.assertions(2);
    const ERROR_MESSAGE = 'Something went wrong';
    const { queryByText, rerender } = render(
      <InlineEdit errorMessage={null} />
    );
    expect(queryByText(ERROR_MESSAGE)).not.toBeInTheDocument();

    rerender(<InlineEdit errorMessage={ERROR_MESSAGE} />);
    expect(queryByText(ERROR_MESSAGE)).toBeInTheDocument();
  });

  it('should show input field when static text is clicked', () => {
    expect.assertions(2);
    const { container } = render(<InlineEdit text='Hello world' />);
    const displayTextElement = container.querySelector(
      '.InlineEdit-displayText'
    );

    expect(container.querySelector('input')).toEqual(null);
    fireEvent.click(displayTextElement);
    expect(container.querySelector('input')).not.toEqual(null);
  });

  it('should submit on enter', () => {
    const submitHandler = jest.fn();
    const { container } = render(
      <InlineEdit text='Hello world' onSubmit={submitHandler} />
    );

    fireEvent.click(container.querySelector('.InlineEdit-displayText'));

    const input = container.querySelector('input');
    fireEvent.change(input, { target: { value: 'Have a good time' } });
    fireEvent.keyDown(input, { nativeEvent: { keyCode: 13 } });
    expect(submitHandler).toBeCalled();
  });

  it('should submit on blur', () => {
    const submitHandler = jest.fn();
    const { container } = render(
      <InlineEdit text='Hello world' onSubmit={submitHandler} />
    );

    fireEvent.click(container.querySelector('.InlineEdit-displayText'));

    const input = container.querySelector('input');
    fireEvent.change(input, { target: { value: 'Have a good time' } });
    fireEvent.blur(input);
    expect(submitHandler).toBeCalled();
  });

  it('should NOT submit if nothing is changed', () => {
    const submitHandler = jest.fn();
    const { container } = render(
      <InlineEdit text='Hello world' onSubmit={submitHandler} />
    );

    fireEvent.click(container.querySelector('.InlineEdit-displayText'));

    const input = container.querySelector('input');
    // The end value is equal to initial
    fireEvent.change(input, { target: { value: 'Hello world' } });
    fireEvent.keyDown(input, { nativeEvent: { keyCode: 13 } });
    expect(submitHandler).not.toBeCalled();
  });
});
