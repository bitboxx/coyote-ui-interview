import someAsyncCall, { ERROR_MESSAGE } from './';

describe('someAsyncCall', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  it('should resolve if payload is "Thanks for all the fish"', () => {
    expect.assertions(1);
    const promise = someAsyncCall('Thanks for all the fish');
    jest.advanceTimersByTime(3000);
    expect(promise).resolves.toBeUndefined();
  });

  it.each([['Hello World'], ['Something else']])(
    'should reject if payload is "%s"',
    (payload) => {
      expect.assertions(1);
      const promise = someAsyncCall(payload);
      jest.advanceTimersByTime(3000);
      expect(promise).rejects.toEqual(ERROR_MESSAGE);
    }
  );
});
