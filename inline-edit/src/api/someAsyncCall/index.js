const ERROR_MESSAGE =
  'Oops! Something has gone terribly wrong! Hint: "Thanks for all the fish"';
const someAsyncCall = (payload) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (payload === 'Thanks for all the fish') {
        resolve();
      } else {
        reject(ERROR_MESSAGE);
      }
    }, 2500);
  });

export default someAsyncCall;
export { ERROR_MESSAGE };
