import React, { Component } from 'react';

import someAsyncCall from './api/someAsyncCall';
import InlineEdit, { status } from './components/InlineEdit/index.jsx';

import './App.css';

const DEFAULT_TEXT = 'Hello world';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: DEFAULT_TEXT,
      status: status.PRISTINE,
      errorMessage: null,
    };
  }

  onSubmitHandler = async (event) => {
    let nextStatus = status.LOADING;
    let errorMessage = null;
    let text = event.target.value;

    this.setState({ status: nextStatus });

    try {
      await someAsyncCall(text);
      nextStatus = status.SUCCESS;
    } catch (error) {
      text = DEFAULT_TEXT;
      errorMessage = error;
      nextStatus = status.ERROR;
    }

    this.setState({ status: nextStatus, errorMessage, text });
  };

  render() {
    return (
      <div className='App'>
        <div className='App-card'>
          <InlineEdit {...this.state} onSubmit={this.onSubmitHandler} />
        </div>
      </div>
    );
  }
}

export default App;
