import React from 'react';
import { render } from '@testing-library/react';

import someAsyncCall from './api/someAsyncCall';
import App from './App';

jest.mock('./api/someAsyncCall');
jest.mock('./components/InlineEdit/index.jsx');

describe('App', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  it('should render', () => {
    const { container } = render(<App />);

    expect(container).toMatchSnapshot();
  });

  it('should call someAsyncCall on submit', async () => {
    const app = new App();

    app.setState = () => {};
    await app.onSubmitHandler({ target: { value: 'Hello' } });

    expect(someAsyncCall).toBeCalled();
  });
});
